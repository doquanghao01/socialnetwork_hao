import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/account.dart';

import 'follow.dart';

class AccountDetails extends StatelessWidget {
  AccountDetails({Key? key, required this.account}) : super(key: key);
  Account account;
  var img = [
    'https://topshare.vn/wp-content/uploads/2021/10/hinh-anh-nen-anime-phong-canh-dep-35.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZzkUrCSVkfBKZfOPI1OLoMbOEdhy-aFKh0sDBAAXvjN8IyPvCJi4nsEk4JM3vpJGWWfo&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQtV9h2YCS2g8t-P3Ra1Mst1VJ23arPJx8YXiVcQY-be6NUVVxlRNjm-knO1qY-35elebk&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyBRNxcmlwBqVQyRJ1AaVRgidtDs5jfeVELkBxF6aBmLps8WHzDIR5bIaP7ovL-_QvtTc&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMmJbZL_t2hbWcoczkMXGNBuXLqvnsFMhtgQ&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4SNJ1E1eh2ukdHFCUckPHVUH6u9KwkqpEwojJakoqKpi_a9FLk545f9S2Uh1AFMdAuCc&usqp=CAU',
    'https://i.pinimg.com/474x/b1/d6/cf/b1d6cff4b2c77fbd200b97874329e72d.jpg',
    'https://img5.thuthuatphanmem.vn/uploads/2021/11/26/anh-phong-canh-co-trang-trung-quoc-buon-tuyet-dep_010556327.jpg'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Container(
                    alignment: Alignment.topLeft,
                    child: const Icon(
                      Icons.chevron_left,
                      size: 30,
                      color: Colors.black,
                    )),
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 40,
                    backgroundImage: NetworkImage(account.img),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            account.name,
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 25),
                          ),
                          Text(
                            'April 12th',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black.withOpacity(0.5)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 1,
                        height: 50,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(25),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AccountDetails(
                                    account: account,
                                  ),
                                ),
                              );
                            },
                            child: const Text(
                              "Connect",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '105k',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black.withOpacity(1)),
                            ),
                            Text(
                              'Fllowers',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black.withOpacity(1)),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '45',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black.withOpacity(1)),
                          ),
                          Text(
                            'Following',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.black.withOpacity(1)),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Posts",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 30,
              ),
              Expanded(
                child: GridView.builder(
                    padding: EdgeInsets.zero,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    itemCount: img.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              image: NetworkImage(img[index]),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      );
                    }),
              )
            ],
          )),
    );
  }
}
