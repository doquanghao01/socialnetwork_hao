import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/account.dart';

import '../signinOrregister/TextFieldContainer.dart';

class BlogDetails extends StatefulWidget {
  BlogDetails({Key? key, required this.account}) : super(key: key);
  Account account;
  @override
  State<BlogDetails> createState() => _BlogDetailsState();
}

class _BlogDetailsState extends State<BlogDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: const EdgeInsets.only(top: 40, left: 10, right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: CircleAvatar(
                        radius: 20,
                        backgroundImage: NetworkImage(widget.account.img),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.account.name,
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                            const Text('Surat, Gujarat'),
                          ],
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Text("12 min"),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 10),
                child: Image(
                  image: NetworkImage(widget.account.img),
                  width: double.maxFinite,
                  fit: BoxFit.fitWidth,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: [
                    const Icon(Icons.favorite_outline),
                    const Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Icon(Icons.chat_bubble_outline),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Icon(Icons.reply_outlined),
                    ),
                    Expanded(
                      child: Container(
                        alignment: Alignment.bottomRight,
                        child: const Text("7,327 views"),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      '- Free is Ipsum 15th the variations a alteration attributed with 15th There even by there century graphic.',
                      style: TextStyle(fontSize: 18),
                    ),
                    const Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'Have of designs, true ',
                            style: TextStyle(fontSize: 18),
                          ),
                          WidgetSpan(child: Icon(Icons.tag_faces)),
                          TextSpan(
                            text: ' are Ipsum De out ',
                            style: TextStyle(fontSize: 18),
                          ),
                          WidgetSpan(
                            child: Icon(Icons.tag_faces),
                            style: TextStyle(fontSize: 18),
                          ),
                          TextSpan(
                            text: ' are ',
                            style: TextStyle(fontSize: 18),
                          ),
                          WidgetSpan(child: Icon(Icons.tag_faces)),
                          TextSpan(
                            text:
                                ' Internet predefined alteration bonorum free specimen.',
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: Text(
                        "View all 28 comments",
                        style: TextStyle(
                            fontSize: 18,
                            color: Color.fromARGB(255, 122, 122, 122)),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          width: 280,
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(255, 236, 236, 236),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: const TextField(
                            decoration: InputDecoration(
                              hintText: "Comment me",
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text(
                            "Post",
                            style: TextStyle(fontSize: 20, color: Colors.blue),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
