import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../modle/account.dart';

class Friend extends StatefulWidget {
  const Friend({Key? key}) : super(key: key);

  @override
  State<Friend> createState() => _FriendState();
}

class _FriendState extends State<Friend> {
  List<Account> listAccount = [
    Account(
        1,
        'Huyền cute',
        'https://1.bp.blogspot.com/-bhApLXUh59Y/Xo8mwHtO9TI/AAAAAAAAb3o/rNfRFfoati8bOXyQJO8EUaSciPAW9xdHgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25285%2529.jpg',
        1,
        1),
    Account(
        2,
        'Nguyễn Ngọc Ánh',
        'https://1.bp.blogspot.com/-jepOpmpRKM4/Xo8nVl82ymI/AAAAAAAAb6Y/nylx_U7kSmMOM7FZBwZfXpKePEMJ6l2VQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25289%2529.jpg',
        1,
        0),
    Account(
        3,
        'Đỗ Thùy Dung',
        'https://1.bp.blogspot.com/-oAyGszBIxVI/Xo8l1TtLdaI/AAAAAAAAb1w/kkp63meW95c0F6iCDyDzEy5-5HyQhtYpQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252821%2529.jpg',
        1,
        0),
    Account(
        0,
        'Nguyễn Hoài An',
        'https://1.bp.blogspot.com/-oYX9NGH3RBs/Xo8mhiYq8qI/AAAAAAAAb2k/S6SLW8crrM4SZBdgYxamCDYsZqqgpug8QCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252834%2529.jpg',
        0,
        0),
    Account(
        1,
        'Nguyễn Gia Hân',
        'https://1.bp.blogspot.com/-BlQi2_U9ga8/Xo8mo8E3VzI/AAAAAAAAb2w/ZLJWjryxFM0dXArOHUm_aadJHcxNY3kOQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252837%2529.jpg',
        0,
        0),
    Account(
        1,
        'Đỗ Thảo Linh',
        'https://1.bp.blogspot.com/-JLTwDYM_DlU/Xo8msGuO9jI/AAAAAAAAb3Q/83K-W4yctIw_F-b_hYvhlzLL20E0CcSbQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252844%2529.jpg',
        0,
        0),
    Account(
        1,
        'Lê Thanh Mai',
        'https://1.bp.blogspot.com/-bl32BAJfpTU/Xo8mtdirnOI/AAAAAAAAb3Y/R6oFS8ap4SoofEqeVgMBi1nu0I9iUQ_pgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252846%2529.jpg',
        0,
        0),
    Account(
        1,
        'Phùng Thị Tuyết Lan',
        'https://1.bp.blogspot.com/-RtOgsXnh36M/Xo8mxmMKOXI/AAAAAAAAb3s/dmUncg_wt8skpSIu5ataGWbUDb5Y6LvGgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252850%2529.jpg',
        0,
        0)
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.black.withOpacity(0.4),
                radius: 30,
                child: CircleAvatar(
                  radius: 27,
                  backgroundColor: Colors.white,
                  child: Icon(
                    Icons.person_add_outlined,
                    size: 35,
                    color: Colors.black.withOpacity(0.4),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Follow Requests',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.black.withOpacity(0.4)),
                      ),
                      Text(
                        'Approve or ignore request',
                        style: TextStyle(
                            fontSize: 20, color: Colors.black.withOpacity(0.4)),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Today',
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black.withOpacity(0.5)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              children: [
                Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 10),
                      child: const CircleAvatar(
                        backgroundColor: Color.fromARGB(255, 218, 218, 218),
                        radius: 30,
                        child: CircleAvatar(
                          radius: 27,
                          backgroundImage: NetworkImage(
                              "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
                        ),
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 25, top: 10),
                      child: const CircleAvatar(
                        backgroundColor: Color.fromARGB(255, 218, 218, 218),
                        radius: 30,
                        child: CircleAvatar(
                          radius: 27,
                          backgroundImage: NetworkImage(
                              "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: 'amani.buchanan, layton_beck',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black.withOpacity(0.6),
                            ),
                          ),
                          TextSpan(
                            text: ' and',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black.withOpacity(0.4),
                            ),
                          ),
                          TextSpan(
                            text: ' 5 others',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black.withOpacity(0.6),
                            ),
                          ),
                          TextSpan(
                            text: ' started following you. 2d',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black.withOpacity(0.4),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Recent',
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black.withOpacity(0.5)),
            ),
          ),
          ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: 2,
            shrinkWrap: true,
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.only(top: 10, left: 10),
              child: Row(
                children: [
                  const CircleAvatar(
                    backgroundColor: Color.fromARGB(255, 218, 218, 218),
                    radius: 30,
                    child: CircleAvatar(
                      radius: 27,
                      backgroundImage: NetworkImage(
                          "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: 'Follow',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black.withOpacity(0.4),
                              ),
                            ),
                            TextSpan(
                              text: ' Leonardo Carty, Reilly Sadler',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black.withOpacity(0.6),
                              ),
                            ),
                            TextSpan(
                              text:
                                  ' and others you know to see their photos and videos. 5w',
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black.withOpacity(0.4),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Text(
              'Suggestions',
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  color: Colors.black.withOpacity(0.5)),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: List.generate(
                  listAccount.length,
                  (index) => Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 60,
                          height: 60,
                          child: Stack(
                            children: [
                              CircleAvatar(
                                backgroundColor: const Color.fromARGB(255, 212, 212, 212),
                                radius: 40,
                                child: CircleAvatar(
                                  radius: 28,
                                  backgroundImage:
                                      NetworkImage(listAccount[index].img),
                                ),
                              ),
                              Visibility(
                                visible: listAccount[index].statusonoff == 1
                                    ? true
                                    : false,
                                child: Positioned(
                                  top: 43,
                                  left: 45,
                                  child: Container(
                                    width: 13,
                                    height: 13,
                                    decoration: const BoxDecoration(
                                        color: Colors.green,
                                        shape: BoxShape.circle),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  listAccount[index].name,
                                  style: const TextStyle(
                                      fontSize: 20, fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  'Matt Hayden',
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black.withOpacity(0.5)),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 80,
                          height: 35,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: ElevatedButton(
                              onPressed: () {},
                              child: const Text(
                                "Follow",
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text('x',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.black.withOpacity(0.5)),),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
