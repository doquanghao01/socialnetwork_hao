import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/MyHomePage/blogdetails.dart';
import 'package:socialnetwork_hao/MyHomePage/news.dart';
import 'package:socialnetwork_hao/modle/account.dart';

import 'follow.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class Comment {
  String name;
  String comment;
  Comment(this.name, this.comment);
}

class _HomePageState extends State<HomePage> {
  List<Account> listAccount = [
    Account(
        1,
        'Huyền cute',
        'https://1.bp.blogspot.com/-bhApLXUh59Y/Xo8mwHtO9TI/AAAAAAAAb3o/rNfRFfoati8bOXyQJO8EUaSciPAW9xdHgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25285%2529.jpg',
        1,
        1),
    Account(
        2,
        'Nguyễn Ngọc Ánh',
        'https://4.bp.blogspot.com/-qNzhEXcSAjY/XJSH4PZZBuI/AAAAAAAArGA/eSeTQ6ieqp0rFJV0vFkMSJzzbwmvlkUHwCLcBGAs/s1600/53666494_2352017985084086_3639378295323099136_o.jpg',
        1,
        0),
    Account(
        3,
        'Đỗ Thùy Dung',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1ilmTeNV-QDn18ASbzFlHhHrr9PFE3aUQrA&usqp=CAU',
        1,
        0),
    Account(
        0,
        'Nguyễn Hoài An',
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUeFRuXJAZSgOXPjDkwjQmY2HL1Bhm4gymuS-tLSoguI1f_rkhIZTmaBZspjA2jkbh_5s&usqp=CAU',
        0,
        0),
    Account(
        1,
        'Nguyễn Gia Hân',
        'https://allimages.sgp1.digitaloceanspaces.com/photographercomvn/2021/05/1620302109_157_Top-99-hinh-anh-hot-girl-xinh-me-hon-quen.jpg',
        0,
        0),
    Account(
        1,
        'Đỗ Thảo Linh',
        'https://kenh14cdn.com/203336854389633024/2021/5/27/thaonhile1900664993059306143693438777978355156364987n-162207545142142638700.jpg',
        0,
        0),
    Account(
        1,
        'Lê Thanh Mai',
        'https://1.bp.blogspot.com/-bl32BAJfpTU/Xo8mtdirnOI/AAAAAAAAb3Y/R6oFS8ap4SoofEqeVgMBi1nu0I9iUQ_pgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252846%2529.jpg',
        0,
        0),
    Account(
        1,
        'Phùng Thị Tuyết Lan',
        'https://cdn.24h.com.vn/upload/2-2022/images/2022-05-16/Co-gai-noi-bat-o-bai-bien-Hawaii-nho-mac-ton-dac-diem-279266997_512316893924037_3784097687237503646_n-1652668656-488-width1000height1250.jpg',
        0,
        0)
  ];
  List listComment = [
    Comment('Nola Padilla', 'Humour, it use used middle.'),
    Comment('Kristie Smith', 'Sentence free chunks is in.')
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 60),
        child: Column(
          children: [
            Top(listAccount: listAccount),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: listAccount.length,
                itemBuilder: (context, index) => Container(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.maxFinite,
                        height: 0.5,
                        color: const Color.fromARGB(255, 196, 196, 196),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: InkWell(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Follow(
                                        account: listAccount[index]),
                                  ),
                                ),
                                child: CircleAvatar(
                                  radius: 20,
                                  backgroundImage:
                                      NetworkImage(listAccount[index].img),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      listAccount[index].name,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const Text('Surat, Gujarat'),
                                  ],
                                ),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(right: 20),
                              child: Text("12 min"),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  BlogDetails(account: listAccount[index]),
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 15, bottom: 10),
                          child: Image(
                            image: NetworkImage(listAccount[index].img),
                            width: double.maxFinite,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5, right: 20),
                        child: Row(
                          children: const [
                            Stackimg(),
                            Expanded(
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Text(
                                  "21 Like this",
                                  style: TextStyle(
                                      color:
                                          Color.fromARGB(255, 105, 105, 105)),
                                ),
                              ),
                            ),
                            Icon(
                              Icons.more_vert,
                              color: Color.fromARGB(255, 105, 105, 105),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10, left: 15),
                        width: double.maxFinite,
                        height: 50,
                        child: ListView.builder(
                          padding: EdgeInsets.zero,
                          itemCount: 2,
                          shrinkWrap: true,
                          itemBuilder: (context, index) => Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Row(
                              children: [
                                Text(
                                  listComment[index].name,
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    listComment[index].comment,
                                    style: const TextStyle(
                                        color:
                                            Color.fromARGB(255, 145, 144, 144),
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Stackimg extends StatelessWidget {
  const Stackimg({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 10),
          child: const CircleAvatar(
            backgroundColor: Color.fromARGB(255, 218, 218, 218),
            radius: 15,
            child: CircleAvatar(
              radius: 13,
              backgroundImage: NetworkImage(
                  "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 30),
          child: const CircleAvatar(
            backgroundColor: Color.fromARGB(255, 218, 218, 218),
            radius: 15,
            child: CircleAvatar(
              radius: 13,
              backgroundImage: NetworkImage(
                  "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 50),
          child: const CircleAvatar(
            backgroundColor: Color.fromARGB(255, 218, 218, 218),
            radius: 15,
            child: CircleAvatar(
              radius: 13,
              backgroundImage: NetworkImage(
                  "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
            ),
          ),
        ),
      ],
    );
  }
}

class Top extends StatelessWidget {
  const Top({
    Key? key,
    required this.listAccount,
  }) : super(key: key);

  final List<Account> listAccount;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: SizedBox(
                width: 60,
                height: 60,
                child: Stack(
                  children: [
                    const CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 40,
                      child: CircleAvatar(
                        radius: 28,
                        backgroundImage: NetworkImage(
                            "https://i.pinimg.com/564x/82/0e/9f/820e9fc6b65a11d6679af65d9ee62654.jpg"),
                      ),
                    ),
                    Positioned(
                      top: 40,
                      left: 40,
                      child: Container(
                        width: 20,
                        height: 20,
                        decoration: const BoxDecoration(
                            color: Colors.blue, shape: BoxShape.circle),
                        child: const Icon(Icons.add,
                            size: 21, color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Row(
              children: List.generate(
                listAccount.length,
                (index) => Container(
                  margin: const EdgeInsets.all(8),
                  width: 65,
                  height: 65,
                  child: Stack(
                    children: [
                      CircleAvatar(
                        backgroundColor: listAccount[index].statusonoff == 1
                            ? Colors.red
                            : const Color.fromARGB(255, 218, 218, 218),
                        radius: 40,
                        child: InkWell(
                          onTap: () => listAccount[index].statusonoff == 1
                              ? Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => News(
                                      account: listAccount[index],
                                    ),
                                  ),
                                )
                              : () => context,
                          child: CircleAvatar(
                            radius: 30,
                            backgroundImage:
                                NetworkImage(listAccount[index].img),
                          ),
                        ),
                      ),
                      Visibility(
                        visible:
                            listAccount[index].statuslive == 1 ? true : false,
                        child: Positioned(
                          left: 35,
                          child: Container(
                            width: 30,
                            height: 16,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: const Center(
                              child: Text(
                                'Live',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 13),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
