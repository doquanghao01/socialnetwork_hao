import 'package:flutter/material.dart';
import 'package:socialnetwork_hao/MyHomePage/account.dart';
import 'package:socialnetwork_hao/MyHomePage/friend.dart';
import 'package:socialnetwork_hao/MyHomePage/homepase.dart';
import 'package:socialnetwork_hao/MyHomePage/seach.dart';

import '../modle/account.dart';

class MainHome extends StatefulWidget {
  const MainHome({Key? key}) : super(key: key);

  @override
  State<MainHome> createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> with TickerProviderStateMixin {
  late TabController _tabController;
  @override
  initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: _tabController,
        children: const [HomePage(), Seach(), Friend(), uiAccount()],
      ),
      bottomNavigationBar: Container (
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: TabBar(
          controller: _tabController,
          labelColor: Colors.blue,
          unselectedLabelColor: const Color.fromARGB(255, 172, 172, 172),
          indicator: CircleTabIndicatro(color: Colors.blue, radius: 4),
          indicatorSize: TabBarIndicatorSize.label,
          tabs: const [
            Tab(
              icon: Icon(Icons.home),
            ),
            Tab(
              icon: Icon(Icons.search),
            ),
            Tab(
              icon: Icon(Icons.timeline_outlined),
            ),
            Tab(
              icon: Icon(Icons.person),
            ),
          ],
        ),
      ),
    );
  }
}

class CircleTabIndicatro extends Decoration {
  final Color color;
  double radius;
  CircleTabIndicatro({required this.color, required this.radius});
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _CirclePainter(color: color, radius: radius);
  }
}

class _CirclePainter extends BoxPainter {
  final Color color;
  double radius;
  _CirclePainter({required this.color, required this.radius});
  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    Paint _paint = Paint();
    _paint.color = color;
    _paint.isAntiAlias = true;
    final Offset circleOffset = Offset(configuration.size!.width/ 1.7 -radius/2, configuration.size!.height-radius);
    canvas.drawCircle(offset+circleOffset, radius, _paint);
  }
}
