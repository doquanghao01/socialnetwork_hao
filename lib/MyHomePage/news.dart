import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:socialnetwork_hao/modle/account.dart';

import '../signinOrregister/TextFieldContainer.dart';

class News extends StatefulWidget {
  News({Key? key, required this.account}) : super(key: key);
  Account account;

  @override
  State<News> createState() => _NewsState();
}

class _NewsState extends State<News> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: const EdgeInsets.only(top: 50, left: 10, right: 10),
      child: Column(
        children: [
          const LinearProgressIndicator(
            value: 0.2,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: CircleAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage(widget.account.img),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: widget.account.name,
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              const WidgetSpan(
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10),
                                ),
                              ),
                              const TextSpan(
                                text: '10 min',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                        ),
                        const Text('106 views'),
                      ],
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Icon(
                    Icons.more_vert,
                    color: Color.fromARGB(255, 105, 105, 105),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Image.network(
                widget.account.img,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Row(
            children: const [
              Expanded(
                child: TextFieldContainer(
                  child: TextField(
                    decoration: InputDecoration(
                      hintText: "Send message",
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Icon(Icons.send),
            ],
          )
        ],
      ),
    ));
  }
}
