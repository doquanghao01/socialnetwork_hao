import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../modle/account.dart';
import '../signinOrregister/TextFieldContainer.dart';

class Seach extends StatefulWidget {
  const Seach({Key? key}) : super(key: key);

  @override
  State<Seach> createState() => _SeachState();
}

class _SeachState extends State<Seach> {
  List<Account> listAccount = [
    Account(
        1,
        'Huyền cute',
        'https://1.bp.blogspot.com/-bhApLXUh59Y/Xo8mwHtO9TI/AAAAAAAAb3o/rNfRFfoati8bOXyQJO8EUaSciPAW9xdHgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25285%2529.jpg',
        1,
        1),
    Account(
        2,
        'Nguyễn Ngọc Ánh',
        'https://1.bp.blogspot.com/-jepOpmpRKM4/Xo8nVl82ymI/AAAAAAAAb6Y/nylx_U7kSmMOM7FZBwZfXpKePEMJ6l2VQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%25289%2529.jpg',
        1,
        0),
    Account(
        3,
        'Đỗ Thùy Dung',
        'https://1.bp.blogspot.com/-oAyGszBIxVI/Xo8l1TtLdaI/AAAAAAAAb1w/kkp63meW95c0F6iCDyDzEy5-5HyQhtYpQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252821%2529.jpg',
        1,
        0),
    Account(
        0,
        'Nguyễn Hoài An',
        'https://1.bp.blogspot.com/-oYX9NGH3RBs/Xo8mhiYq8qI/AAAAAAAAb2k/S6SLW8crrM4SZBdgYxamCDYsZqqgpug8QCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252834%2529.jpg',
        0,
        0),
    Account(
        1,
        'Nguyễn Gia Hân',
        'https://1.bp.blogspot.com/-BlQi2_U9ga8/Xo8mo8E3VzI/AAAAAAAAb2w/ZLJWjryxFM0dXArOHUm_aadJHcxNY3kOQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252837%2529.jpg',
        0,
        0),
    Account(
        1,
        'Đỗ Thảo Linh',
        'https://1.bp.blogspot.com/-JLTwDYM_DlU/Xo8msGuO9jI/AAAAAAAAb3Q/83K-W4yctIw_F-b_hYvhlzLL20E0CcSbQCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252844%2529.jpg',
        0,
        0),
    Account(
        1,
        'Lê Thanh Mai',
        'https://1.bp.blogspot.com/-bl32BAJfpTU/Xo8mtdirnOI/AAAAAAAAb3Y/R6oFS8ap4SoofEqeVgMBi1nu0I9iUQ_pgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252846%2529.jpg',
        0,
        0),
    Account(
        1,
        'Phùng Thị Tuyết Lan',
        'https://1.bp.blogspot.com/-RtOgsXnh36M/Xo8mxmMKOXI/AAAAAAAAb3s/dmUncg_wt8skpSIu5ataGWbUDb5Y6LvGgCLcBGAsYHQ/s1600/Anh-gai-xinh-deo-kinh-2k%2B%252850%2529.jpg',
        0,
        0)
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 40),
      child: Column(children: [
        const TextFieldContainer(
          child: TextField(
            decoration: InputDecoration(
              icon: Icon(
                Icons.search,
                color: Color.fromARGB(255, 126, 126, 126),
              ),
              hintText: "Search messages",
              border: InputBorder.none,
            ),
          ),
        ),
        Row(
          children: [
            const Text(
              'Recent',
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerRight,
                child: const Text(
                  'Clear',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blue,
                      fontSize: 23),
                ),
              ),
            )
          ],
        ),
        SizedBox(
          height: 100,
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                listAccount.length,
                (index) => Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: Stack(
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 40,
                              child: CircleAvatar(
                                radius: 28,
                                backgroundImage:
                                    NetworkImage(listAccount[index].img),
                              ),
                            ),
                            Visibility(
                              visible: listAccount[index].statusonoff == 1
                                  ? true
                                  : false,
                              child: Positioned(
                                top: 43,
                                left: 45,
                                child: Container(
                                  width: 13,
                                  height: 13,
                                  decoration: const BoxDecoration(
                                      color: Colors.green,
                                      shape: BoxShape.circle),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text(listAccount[index].name),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: const Text(
            'Result',
            style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: List.generate(
                listAccount.length,
                (index) => Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 60,
                        height: 60,
                        child: Stack(
                          children: [
                            CircleAvatar(
                              backgroundColor: const Color.fromARGB(255, 223, 223, 223),
                              radius: 40,
                              child: CircleAvatar(
                                radius: 28,
                                backgroundImage:
                                    NetworkImage(listAccount[index].img),
                              ),
                            ),
                            Visibility(
                              visible: listAccount[index].statusonoff == 1
                                  ? true
                                  : false,
                              child: Positioned(
                                top: 43,
                                left: 45,
                                child: Container(
                                  width: 13,
                                  height: 13,
                                  decoration: const BoxDecoration(
                                      color: Colors.green,
                                      shape: BoxShape.circle),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                listAccount[index].name,
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                '@Lowri12',
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.black.withOpacity(0.5)),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 80,
                        height: 35,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: ElevatedButton(
                            onPressed: () {},
                            child: const Text(
                              "Follow",
                              style: TextStyle(fontSize: 15),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
